<?php

namespace services\modules\v1\models;

use yii\base\ErrorException;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class User extends Base
{

    public static function tableName()
    {
        return '{{%users}}';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    public function getAgentId($user_id)
    {
        $result = (new \yii\db\Query())->select(['email'])->from('users')->where(['id' => $user_id])->one();
        return $result;
    }

    public function checkUserExist($param){
        $check_email = (new \yii\db\Query())->select(['email'])->from('users')->where(['email' => $param['email']])->one();
        if($check_email == ''){
            return ['result' => 'continuesignup'];
        }else{
            return ['result' => 'Email Already exist.'];
        }
    }

    public function sigUpUser($param)
    {          
        $insert = self::getDb()->createCommand()->insert('users', [
                        'first_name' => $param['fname'],
                        'last_name' => $param['lname'],
                        'email' => $param['email'],
                        'password' => $param['password'],
                        'phone' => $param['phone'],
                        'status' => $param['status'],
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();

        $userId = (new \yii\db\Query())->select(['id'])->from('users')->where(['email' => $param['email']])->one();

        $insert_role = self::getDb()->createCommand()->insert('user_roles', [
                        'user_id' => $userId['id'],
                        'role_id' => 2,
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();
        return ['result' => 'User Signup successfully.'];
    }

    public function userlogin($param){

        $userData = (new \yii\db\Query())->select(['id','email','password'])->from('users')->where(['email' => $param['email']])->one();
        if($userData == ''){
                return ['login'=>'false'];
        }else{
                if($userData['email'] == $param['email'] && $userData['password'] == $param['password']){
                        return ['login'=>'true','userid'=>$userData['id']];
                }else{
                        return ['login'=>'false'];
                }
        }
    }

    public function usersforget($param){
        $emailforget = (new \yii\db\Query())->select(['email'])->from('users')->where(['email' => $param['email']])->one();    
         if($emailforget == ''){
                 return ['isEmail'=>'false'];
         }else{

         }       return ['isEmail'=>'true'];
    }
}