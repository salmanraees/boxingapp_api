<?php

namespace services\modules\v1\models;

use yii\base\ErrorException;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Tournament extends Base
{

    public static function tableName()
    {
        return '{{%tournaments}}';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function get_alltournament(){

        $result = (new \yii\db\Query())->select(['id','name'])->from('tournaments')->where(['status' => 1])->all();
        return $result;
    }

    public function add_newTournament($param){
         
         $insert = self::getDb()->createCommand()->insert('tournaments', [
                        'name' => $param['tournament_name'],
                        'status' => $param['status'],
                        'start_date' => $param['start_date'],
                        'finish_date' => $param['end_date'],
                        'type' => $param['tournament_type'],
                        'channel_id' => $param['channel'],
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();

         return ['result' => 'Tournament add successfully.'];
    }

    public function upComming_events(){

        $where = 'matches.is_featured = 1';
        $where .= ' AND tournaments.start_date > CURDATE()';
        $result = self::find()
            ->select('tournaments.id, tournaments.name, tournaments.start_date, boxerone.name AS boxername_first, boxertwo.name AS boxername_second, channel.name AS channel_name')
            ->innerJoin('matches', 'tournaments.id = matches.tournament_id')
            ->innerJoin('boxers boxerone', 'boxerone.id = matches.first_boxer_id')
            ->innerJoin('boxers boxertwo', 'boxertwo.id = matches.second_boxer_id')
            ->innerJoin('ref_channels channel', 'tournaments.channel_id = channel.id')
            ->where($where)
            ->asArray()
            ->all();

        return $result;
    }
}