<?php

namespace services\modules\v1\models;

use yii\base\ErrorException;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Match extends Base
{

    public static function tableName()
    {
        return '{{%matches}}';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

   public function add_newMatch($param){
         
         if($param['boxer_first'] == $param['boxer_second']){
            return ['result' => 'Select diffrenet boxers name'];
         }else{
         $insert = self::getDb()->createCommand()->insert('matches', [
                        'first_boxer_id' => $param['boxer_first'],
                        'second_boxer_id' => $param['boxer_second'],
                        'tournament_id' => $param['tournament'],
                        'venue' => $param['venu'],
                        'start_date' => $param['start_date'],
                        'finish_date' => $param['end_date'],
                        'reminder_note' => $param['reminder_note'],
                        'pre_match_summary' => $param['pre_match_summary'],
                        'post_match_summary' => $param['post_match_summary'],
                        'match_winner' => $param['winner'],
                        'is_featured' => $param['featured_match'],
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();

         $lastId = self::getDb()->getLastInsertID();
         $channeId = (new \yii\db\Query())->select(['channel_id'])->from('tournaments')->where(['id' => $param['tournament']])->one();
          $insert_channel = self::getDb()->createCommand()->insert('matche_channels', [
                        'match_id' => $lastId,
                        'channel_id' => $channeId['channel_id'],
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();

           return ['result' => 'Match add successfully.'];
        }
    }
}