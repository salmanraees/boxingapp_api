<?php

namespace services\modules\v1\models;

use yii\base\ErrorException;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Boxer extends Base
{

    public static function tableName()
    {
        return '{{%boxers}}';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function get_allboxers(){

        $result = (new \yii\db\Query())->select(['id','name'])->from('boxers')->where(['status' => 1])->all();
        return $result;
    }

    public function add_newboxer($param)
    {          
        $insert = self::getDb()->createCommand()->insert('boxers', [
                        'name' => $param['boxersName'],
                        'match_count' => $param['totalmatch'],
                        'win_count' => $param['winmatch'],
                        'loss_count' => $param['lossmatch'],
                        'nr_count' => $param['nrmatch'],
                        'knockout_count' => $param['knockoutmatch'],
                        'status' => $param['status'],
                        'created_at' => new Expression('NOW()'),
                        'updated_at' => new Expression('NOW()'),
                    ])->execute();

        return ['result' => 'Boxer add successfully.'];
    }
}