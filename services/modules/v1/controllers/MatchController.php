<?php
namespace services\modules\v1\controllers;

use Aws\Api\Service;
use Yii;
use services\modules\v1\models\Match;
use yii\db\Expression;
use yii\web\NotFoundHttpException;


class MatchController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['addmatch'];
        return $behaviors;
    }

    public function actionAddmatch(){
        $param = Yii::$app->request->post();
        $insert_model = new Match();
        $result = $insert_model->add_newMatch($param);
        return $result;
    }
}