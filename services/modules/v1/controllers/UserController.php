<?php
namespace services\modules\v1\controllers;

use Aws\Api\Service;
use Yii;
use services\modules\v1\models\User;
use yii\db\Expression;
use yii\web\NotFoundHttpException;


class UserController extends BaseController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['usersignup','userlogin'];
        return $behaviors;
    }

    public function actionGetuser()
    {
        $id = Yii::$app->request->get('id');
        $model = new User();
        $order =  $model->getAgentId($id);
        return $order;
    }

    public function actionUsersignup()
    {
        $param = Yii::$app->request->post();
        $insert_model = new User();
        $result = $insert_model->checkUserExist($param);
        if($result['result'] == 'continuesignup'){
            $success = $insert_model->sigUpUser($param);
            return $success;
        }else{  
            return $result;
        }
   }

   public function actionUserlogin(){

        $param = yii::$app->request->post();
        $checklogin = new User();
        $check_user_login = $checklogin->userlogin($param);
        return $check_user_login;
   }

   public function actionUserforget(){

        $param = yii::$app->request->post();
        $forgetemail = new User();
        $resutlt = $forgetemail->usersforget($param);
        return $resutlt;

   }
}